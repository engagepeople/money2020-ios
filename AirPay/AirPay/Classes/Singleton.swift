//
//  Singleton.swift
//  AirPay
//
//  Created by Ivan Carosati on 2018-10-21.
//  Copyright © 2018 Ivan Carosati. All rights reserved.
//

import Foundation

class Singleton {
	
	// MARK: - Properties
	
	private static var sharedSingleton: Singleton = {
		let singleton = Singleton()
		
		// Configuration
		// ...
		
		return singleton
	}()
	
	// MARK: - Vars
	let token: String;
	
	
	// Initialization
	private init() {
//		self.token = "DNNMCNDA" // VISA
		self.token = "PAYPALTK" // PAYPAL
	}
	
	// MARK: - Accessors
	
	class func shared() -> Singleton {
		return sharedSingleton
	}
	
}
