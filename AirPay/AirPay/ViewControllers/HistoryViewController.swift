//
//  HistoryViewController.swift
//  AirPay
//
//  Created by Ivan Carosati on 2018-10-20.
//  Copyright © 2018 Ivan Carosati. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "History"
		navigationController?.navigationBar.prefersLargeTitles = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HistoryViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		print("Selected \(indexPath.row)")
		
		tableView.deselectRow(at: indexPath, animated: true)
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		
	}
}

extension HistoryViewController : UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 6
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		switch indexPath.row {
		case 0:
			// Coffee
			let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell", for: indexPath) as! HistoryTableViewCell
			cell.logoImageView.image = UIImage.init(named: "icon-coffee")
			cell.nameLbl.text = "Coffee Machine"
			cell.timeLbl.text = "2 min ago"
			cell.textLbl.text = "You've earned 200 pts on your $2.99 purchase."
			return cell
			
		case 1:
			// Promo
			let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryPromoTableViewCell", for: indexPath) as! HistoryPromoTableViewCell
			cell.nameLbl.text = "Promotion"
			cell.timeLbl.text = "1 hour ago"
			cell.textLbl.text = "Double Points: You’ve earned 10,000 pts on your $50.00 purchase."
			return cell
			
		case 2:
			// Coffee
			let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell", for: indexPath) as! HistoryTableViewCell
			cell.logoImageView.image = UIImage.init(named: "icon-mall")
			cell.nameLbl.text = "Downtown Mall"
			cell.timeLbl.text = "Yesterday"
			cell.textLbl.text = "You’ve earned 25,600 pts on your $256.72 purchase."
			return cell
			
		default:
			// Coffee
			let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell", for: indexPath) as! HistoryTableViewCell
			cell.logoImageView.image = UIImage.init(named: "icon-coffee")
			cell.nameLbl.text = "Coffee Machine"
			cell.timeLbl.text = "A long time ago"
			cell.textLbl.text = "You've earned 200 pts on your $2.99 purchase."
			return cell
		}
	}
}
