//
//  AccountViewController.swift
//  AirPay
//
//  Created by Ivan Carosati on 2018-10-20.
//  Copyright © 2018 Ivan Carosati. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {
	
	@IBOutlet var scrollView: UIScrollView!
	
	@IBOutlet weak var contentView: UIView!
	@IBOutlet weak var visaDirectBtn: UIButton!
	@IBOutlet weak var visaDirectLbl: UILabel!
	@IBOutlet weak var visaDirectStatusLbl:UILabel!
	@IBOutlet weak var payPalBtn: UIButton!
	@IBOutlet weak var payPalLbl: UILabel!
	@IBOutlet weak var payPalStatusLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Account"
		navigationController?.navigationBar.prefersLargeTitles = true
		
		self.scrollView.addSubview(self.contentView)
		self.scrollView.contentSize.height = self.contentView.frame.size.height
		
		payPalStatusLbl.text = "Not linked."
		visaDirectStatusLbl.text = "Not linked."
		
		switch (Singleton.shared().token) {
		case "DNNMCNDA": // VISA
			visaDirectStatusLbl.text = "Linked to VISA ending in *** 1234."
			break
			
		case "PAYPALTK": // PAYPAL
			payPalStatusLbl.text = "Linked to icarosati@engagepeople.com"
			break
			
		default:
			// Do nothing.
			break
		}
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
	
	@IBAction func visaDirectTouchDown(sender: UIButton) {
		visaDirectLbl.isHighlighted = true
		visaDirectStatusLbl.isHighlighted = true
	}
	
	@IBAction func visaDirectTouchCancel(sender: UIButton) {
		visaDirectLbl.isHighlighted = false
		visaDirectStatusLbl.isHighlighted = false
	}
	
	@IBAction func visaDirectTouchDragOutside(sender: UIButton) {
		visaDirectLbl.isHighlighted = false
		visaDirectStatusLbl.isHighlighted = false
	}

	@IBAction func visaDirectAction(sender: UIButton) {
		visaDirectLbl.isHighlighted = false
		visaDirectStatusLbl.isHighlighted = false
	}
	
	@IBAction func payPalAction(sender: UIButton) {
		
	}
}
