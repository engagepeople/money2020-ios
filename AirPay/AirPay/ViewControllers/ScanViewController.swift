//
//  ScanViewController.swift
//  AirPay
//
//  Created by Ivan Carosati on 2018-10-20.
//  Copyright © 2018 Ivan Carosati. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import SwiftSpinner

class ScanViewController: UIViewController {
	
	@IBOutlet var topView: UIView!
	@IBOutlet var topTitleLbl: UILabel!
	@IBOutlet var topSubtitleLbl: UILabel!
	
	@IBOutlet var qrOverlayView: UIView!
	
	@IBOutlet var confirmView: UIView!
	@IBOutlet var confirmBackgroundImageView: UIImageView!
	@IBOutlet var confirmCoffeeImageView: UIImageView!
	@IBOutlet var confirmTitleLbl: UILabel!
	@IBOutlet var confirmSubtitleLbl: UILabel!
	@IBOutlet var confirmCancelBtn: UIButton!
	@IBOutlet var confirmBuyBtn: UIButton!
	
	
	@IBOutlet var successView: UIView!
	@IBOutlet var successDismissBtn: UIButton!
	
	var captureSession = AVCaptureSession()
	var previewLayer: AVCaptureVideoPreviewLayer?

	let supportedTypes = [AVMetadataObject.ObjectType.qr]
	var orderId: String = ""
	var isPurchaseInProgress: Bool = false
	

    override func viewDidLoad() {
        super.viewDidLoad()
		
		self.title = "Scan"
//		navigationController?.navigationBar.prefersLargeTitles = true
		self.navigationController?.setNavigationBarHidden(true, animated: false)
		self.navigationController?.setNeedsStatusBarAppearanceUpdate()

        // Start camera.
		let discoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera], mediaType: AVMediaType.video, position: .back)
		guard let device = discoverySession.devices.first else {
			// Oops!
			return
		}
		
		do {
			let input = try AVCaptureDeviceInput(device: device)
			captureSession.addInput(input)
			let metadataOutput = AVCaptureMetadataOutput()
			captureSession.addOutput(metadataOutput)
			
			metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
			metadataOutput.metadataObjectTypes = supportedTypes
		} catch {
			// Ooops!
			return
		}
		
		// Video preview.
		previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
		previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
		previewLayer?.frame = view.layer.bounds
		view.layer.addSublayer(previewLayer!)
		
		qrOverlayView.frame.origin.y = 240
		qrOverlayView.frame.origin.x = (view.bounds.width - qrOverlayView.frame.size.width) / 2
		view.addSubview(topView)
		view.addSubview(qrOverlayView)
		
		// Start
		captureSession.startRunning()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		// Reset all the things!
		confirmView.removeFromSuperview()
		successView.removeFromSuperview()
		topTitleLbl.alpha = 1
		topSubtitleLbl.alpha = 1
		qrOverlayView.alpha = 1
		self.confirmCoffeeImageView.alpha = 1
		self.confirmTitleLbl.alpha = 1
		self.confirmSubtitleLbl.alpha = 1
		self.confirmCancelBtn.alpha = 1
		self.confirmBuyBtn.alpha = 1
		
		isPurchaseInProgress = false
	}
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
	
//	override var preferredStatusBarStyle: UIStatusBarStyle {
//		return .lightContent
//	}
	
	@IBAction func confirmCancelAction(sender: UIButton) {
		UIView.animate(withDuration: 0.2, animations: {
			self.confirmView.alpha = 0
			self.topTitleLbl.alpha = 1
			self.topSubtitleLbl.alpha = 1
			self.qrOverlayView.alpha = 1
		}, completion: { (value: Bool) in
			self.confirmView.removeFromSuperview()
			self.isPurchaseInProgress = false
		})
	}
	
	@IBAction func confirmBuyAction(sender: UIButton) {
		
		SwiftSpinner.show("We're processing your payment..")
		
		let headers: HTTPHeaders = [
			"Auth": Singleton.shared().token
		]
		let params: Parameters = ["order_number": orderId]
		Alamofire.request(
			"http://18.235.170.58/v1/order",
			method: .post,
			parameters: params,
//			encoding: URLEncoding.httpBody,
			headers: headers)
			.validate(statusCode: 200..<300)
			.responseJSON { response in
				
//			print("Request: \(String(describing: response.request))")   // original url request
//			print("Response: \(String(describing: response.response))") // http url response
			print("Result: \(response.result)")                         // response serialization result
				
			SwiftSpinner.hide()
				
			switch response.result {
				case .success:
					print("Validation Successful")
				
					self.successView.alpha = 0
					self.view.addSubview(self.successView)
					UIView.animate(withDuration: 0.2, animations: {
						//			self.confirmView.alpha = 0
						self.confirmCoffeeImageView.alpha = 0;
						self.confirmTitleLbl.alpha = 0
						self.confirmSubtitleLbl.alpha = 0
						self.confirmCancelBtn.alpha = 0
						self.confirmBuyBtn.alpha = 0
						
						self.successView.alpha = 1
					}, completion: { (value: Bool) in
						self.confirmView.removeFromSuperview()
					})
				
				case .failure(let error):
					print(error)
			}
			
//			if let json = response.result.value {
//				print("JSON: \(json)") // serialized json response
//			}
//
//			if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
//				print("Data: \(utf8Text)") // original server data as UTF8 string
//			}
		}
	}
	
	@IBAction func successDismissAction(sender: UIButton) {
		tabBarController?.selectedIndex = 0;
	}
}

extension ScanViewController: AVCaptureMetadataOutputObjectsDelegate {
	func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {

		if (metadataObjects.count == 0) {
			return
		}
		
		let metadata = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
		
		if (supportedTypes.contains(metadata.type)) {
			if (metadata.stringValue != nil) {
				
				orderId = metadata.stringValue!
				print(orderId)
				
				if (!isPurchaseInProgress) {
					isPurchaseInProgress = true
					
					view.addSubview(confirmView)
					confirmView.alpha = 0
					UIView.animate(withDuration: 0.2, animations: {
						self.topTitleLbl.alpha = 0
						self.topSubtitleLbl.alpha = 0
						self.qrOverlayView.alpha = 0
						self.confirmView.alpha = 1
					}, completion: { (value: Bool) in
				
					})
					
				}
			}
		}
	}
	
}
