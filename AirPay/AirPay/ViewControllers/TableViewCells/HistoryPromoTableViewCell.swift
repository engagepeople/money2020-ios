//
//  HistoryPromoTableViewCell.swift
//  AirPay
//
//  Created by Ivan Carosati on 2018-10-21.
//  Copyright © 2018 Ivan Carosati. All rights reserved.
//

import UIKit

class HistoryPromoTableViewCell: UITableViewCell {

	@IBOutlet var nameLbl: UILabel!
	@IBOutlet var timeLbl: UILabel!
	@IBOutlet var textLbl: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
